<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Movie</title>
<style>
<%@ include file="/WEB-INF/css/style.css"%>
</style>
<script type="text/javascript">
function submitForm(page) {
	document.getElementById("page").value = page;
	document.getElementById("movie").submit();
}
</script>
</head>
<body>
	<form action="Movie?" method="post" id="movie">
		<div align="center">
			<h1>SHORT FILM WINDOW</h1>
			<h3>FILTER BY</h3>
			<div>
				<i>Language: </i><br /> <br /> <select name="categoryModel"
					class="dropdown">
					<option value="All">All</option>
					<c:forEach var="categoryModel" items="${listCategory}">
						<option value="${categoryModel}"
							<c:if test="${categoryModel eq selectedCat}">selected="selected"</c:if>>${categoryModel}
					</c:forEach>
				</select>
			</div>
			<br /> <br />
			<div>
				<i>Genre:</i> <br /> <br /> <select name="categoryGenre"
					class="dropdown">
					<option value="All">All</option>
					<c:forEach var="categoryGenre" items="${listGenre}">
						<option value="${categoryGenre}"
							<c:if test="${categoryGenre eq selectedGenre}">selected="selected"</c:if>>${categoryGenre}
					</c:forEach>
				</select>
			</div>
			<br />
			<h3>SORT BY</h3>
			<div>
				<select name="categorySort" class="dropdown">
					<c:forEach var="categorySort" items="${listSortCategory}">
						<option value="${categorySort}"
							<c:if test="${categorySort eq selectedSortCategory}">selected="selected"</c:if>>${categorySort}
					</c:forEach>
				</select>
			</div>
			<br /> <br /> <input type="button" value="Search"
				onclick="submitForm(1)" class="button" /> <br />
			<br />
			<table border="1" cellpadding="5" cellspacing="5" class="table1">
				<tr>
					<th>Movie Id</th>
					<th>Title</th>
					<th>Description</th>
					<th>MovieLength</th>
					<th>MovieReleaseDate</th>
				</tr>

				<c:forEach var="e" items="${listMovies}">
					<tr>
						<td>${e.id}</td>
						<td>${e.title}</td>
						<td>${e.description}</td>
						<td>${e.movieLength}</td>
						<td>${e.movieReleaseDate}</td>
					</tr>
				</c:forEach>
			</table>


			<c:if test="${currentPage != 1}">
				<td><input class="pagination_button" type="button" value="<<"
					onclick="submitForm(${currentPage - 1})" /></td>
			</c:if>
			<!-- For displaying Page numbers. 
    The when condition does not display a link for the current page -->
			<input type="hidden" id="page" name="page" />

			<table border="1" cellpadding="5" cellspacing="5" class="pagination">

				<tr>
					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td>${i}</td>
							</c:when>
							<c:otherwise>
								<td><input class="pagination_buttons" type="button" value="${i}"
									onclick="submitForm(${i})" /></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</table>

			<!--  For displaying Next link -->
			<c:if test="${currentPage lt noOfPages}">
				<td><input class="pagination_button" type="button" value=">>"
					onclick="submitForm(${currentPage + 1})" /></td>
			</c:if>
		</div>
	</form>

</body>
</html>