package com.example.movies;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Movie
 */
@WebServlet("/Movie")
public class Movie extends HttpServlet {

	private static final long serialVersionUID = 1L;
	int recordsPerPage = 10;
	int page = 1;
	public Movie() {
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request = setGenreAndLanguage(request);
		String categoryModel = request.getParameter("categoryModel");
		String categoryGenre = request.getParameter("categoryGenre");
		System.out.println(categoryModel);
		if(categoryModel==null && categoryGenre==null) {
		showAllMovies(request, response);}
		else {showFilteredMovies(request,response);}
	}

	private HttpServletRequest setGenreAndLanguage(HttpServletRequest request) throws ServletException, IOException {
		List<String> listCategory = showAllLanguages(request);
		List<String> listGenre = showAllGenre(request);
		List<String>listSortCategory=showAllSortCategory(request);
		request.setAttribute("listCategory", listCategory);
		request.setAttribute("listGenre", listGenre);
		request.setAttribute("listSortCategory", listSortCategory);
		return request;
	}

	private void showAllMovies(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		MovieDao dao = new MovieDao();
		
		request = setPagination(request);
		String categorySort = request.getParameter("categorySort");
		
		List<MovieModel> listMovies = dao.getAllMovies((page - 1) * recordsPerPage, recordsPerPage, categorySort);
		request.setAttribute("listMovies", listMovies);
		RequestDispatcher view = request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
	}

	private List<String> showAllLanguages(HttpServletRequest request) throws ServletException, IOException {

		MovieDao movieDao = new MovieDao();
		List<String> listCategory = movieDao.getAllLanguages();
		System.out.println(listCategory);
		return listCategory;
	}
	private List<String> showAllSortCategory(HttpServletRequest request) throws ServletException, IOException {

		MovieDao movieDao = new MovieDao();
		List<String> listSortCategory = movieDao.getAllSortCategory();
		System.out.println(listSortCategory);
		return listSortCategory;
	}
	private List<String> showAllGenre(HttpServletRequest request) throws ServletException, IOException {

		MovieDao movieDao = new MovieDao();
		List<String> listGenre = movieDao.getAllGenre();
		System.out.println(listGenre);

		return listGenre;

	}

	private void showFilteredMovies(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String categoryModel = request.getParameter("categoryModel");
		String categorySort = request.getParameter("categorySort");
		String categoryGenre = request.getParameter("categoryGenre");
	
		/*request.setAttribute("categoryModel", categoryModel);
		request.setAttribute("categoryOrder", categoryOrder);
		request.setAttribute("categoryGenre", categoryGenre);*/

		MovieDao movieDao = new MovieDao();
		request = setPagination(request);

		List<MovieModel> listMovies = movieDao.filterMovies(categoryModel, categoryGenre, categorySort,(page - 1) * recordsPerPage, recordsPerPage);
		request.setAttribute("listMovies", listMovies);
		System.out.println(listMovies);
		response.setContentType("text/html;charset=UTF-8");
		request.getRequestDispatcher("index.jsp").include(request, response);
		// dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String categoryModel = request.getParameter("categoryModel");
		request.setAttribute("selectedCat", categoryModel);
		request = setGenreAndLanguage(request);
		String categoryGenre = request.getParameter("categoryGenre");
		request.setAttribute("selectedGenre", categoryGenre);
		String categorySort = request.getParameter("categorySort");
		request.setAttribute("selectedSortCategory", categorySort);
		if ((categoryModel.equals("All") && categoryGenre.equals("All"))) {
			showAllMovies(request, response);
		} else {
			showFilteredMovies(request, response);
		}
	}
	
	public HttpServletRequest setPagination(HttpServletRequest request) {
		MovieDao dao = new MovieDao();
		String categoryModel = request.getParameter("categoryModel");
		String categorySort = request.getParameter("categorySort");
		String categoryGenre = request.getParameter("categoryGenre");
		if (categoryModel == null || (categoryModel.equals("All") && categoryGenre.equals("All"))) {
			categoryModel = "";
			categorySort = "";
			categoryGenre = "";
		}
		System.out.println(categoryModel+ categoryGenre+ categorySort);
		int noOfRecords = dao.getTotalCount(categoryModel, categoryGenre, categorySort);
		System.out.println("noOfRecordss"+ noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		request.setAttribute("noOfPages", noOfPages);
		System.out.println(noOfPages);
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));
		request.setAttribute("currentPage", page);
		return request;
	}

}
