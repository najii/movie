package com.example.movies;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MovieDao {

	Connection con;

	Statement stmt;
	ResultSet rs;

	MovieDao() {

		try {

			Class.forName("com.mysql.jdbc.Driver");

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "root");

			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public List<String> getAllLanguages() {

		List<String> listCategory = new ArrayList<>();
		try {

			String s = "SELECT * FROM category WHERE Type='Language'";
			PreparedStatement preparedStatement = this.con.prepareStatement(s);

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {

				CategoryModel categoryModel = new CategoryModel(rs.getInt("id"), rs.getString("Type"),
						rs.getString("Value"));

				listCategory.add(rs.getString("Value"));

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return listCategory;

	}

	public List<String> getAllGenre() {

		List<String> listCategory = new ArrayList<>();
		try {

			String s = "SELECT * FROM category WHERE Type='Genre'";
			PreparedStatement preparedStatement = this.con.prepareStatement(s);

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {

				listCategory.add(rs.getString("Value"));

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return listCategory;

	}

	public List<String> getAllSortCategory() {
		List<String> listSortCategory = new ArrayList<String>();

		listSortCategory.add("Length");
		listSortCategory.add("ReleaseDate");
		return listSortCategory;
	}

	public int getTotalCount(String language, String genre, String sort) {
		int count = 0;
		try {

			String s;
			System.out.println(language);

			if (language.isEmpty()) {
				s = "select count(*) from movies";
				System.out.println(s);

			} else {
				s = getFilterQuery(language, genre, sort, "Y");
				System.out.println(s);

			}
			PreparedStatement preparedStatement = this.con.prepareStatement(s);
			ResultSet rs = preparedStatement.executeQuery();
			rs.next();
			count = rs.getInt(1);
			System.out.println("da" + count);

		} catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}

	public List<MovieModel> getAllMovies(int offset, int limit, String sort) {
		List<MovieModel> list = new ArrayList<MovieModel>();
		try {

			String s = "select * from movies ";
			s += "ORDER BY " + sort + " DESC LIMIT ? OFFSET ?";
			PreparedStatement preparedStatement = this.con.prepareStatement(s);
			preparedStatement.setInt(1, limit);
			preparedStatement.setInt(2, offset);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {

				MovieModel e = new MovieModel();
				e.setId(rs.getInt("id"));
				e.setTitle(rs.getString("Title"));
				e.setDescription(rs.getString("Description"));
				e.setMovieLength(rs.getString("Length"));
				e.setMovieReleaseDate(rs.getString("ReleaseDate"));
				list.add(e);

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return list;
	}

	public List<MovieModel> filterMovies(String language, String genre, String sort, int page, int recordsPerPage) {

		List<MovieModel> listMovies = new ArrayList<MovieModel>();
		try {

			String s = getFilterQuery(language, genre, sort, "");
			s += "ORDER BY " + sort + " DESC LIMIT  " + recordsPerPage + " OFFSET " + page;
			PreparedStatement preparedStatement = this.con.prepareStatement(s);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {

				MovieModel e = new MovieModel();
				e.setId(rs.getInt("id"));
				e.setTitle(rs.getString("Title"));
				e.setDescription(rs.getString("Description"));
				e.setMovieLength(rs.getString("Length"));
				e.setMovieReleaseDate(rs.getString("ReleaseDate"));
				listMovies.add(e);
				System.out.println(listMovies);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return listMovies;
	}

	public String getFilterQuery(String language, String genre, String sort, String countFlag) {
		String s = "SELECT";
		if (countFlag.isEmpty()) {
			s += " * ";
		} else {
			s += " COUNT(*) ";
		}

		s += "FROM movies WHERE id IN (";

		if (!language.equals("All")) {
			s += "SELECT DISTINCT relationship.Movie_Id FROM relationship,category,movies "
					+ "WHERE relationship.Category_Id= category.id AND category.Type= 'Language' AND category.Value = '"
					+ language + "' AND " + "movies.id = relationship.Movie_Id";
		}
		if (!genre.equals("All")) {
			if (!language.equals("All")) {
				s += " IN (";
			}
			s += "SELECT DISTINCT relationship.Movie_Id FROM relationship,category,movies "
					+ "WHERE relationship.Category_Id= category.id AND  movies.id = relationship.Movie_Id AND category.Type= 'Genre' "
					+ "AND category.Value = '" + genre + "'";
		}
		if (genre.equals("All") || language.equals("All")) {
			s += ") ";
		} else if (!genre.equals("All") && !language.equals("All")) {
			s += ")) ";
		}
		return s;
	}

}
